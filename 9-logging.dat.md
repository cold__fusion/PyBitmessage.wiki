both logfile and keys.dat are located in the subdir mentioned after you use menu FILE / MANAGE KEYS . 

see also:

 * [https://bitmessage.org/forum/index.php/topic,4820.msg11163.html#msg11163](https://bitmessage.org/forum/index.php/topic,4820.msg11163.html#msg11163)

 * http://web.archive.org/web/20170712122006/https://bitmessage.org/forum/index.php/topic,4820.msg11163.html



example content of a **logging.dat** file under /src/:

```
[loggers]
keys = root,null

[handlers]
keys = null

[formatters]
keys = null

[formatter_null]
formatter = null

[logger_root]
level=CRITICAL
handlers=null

[logger_null]
level=CRITICAL
handlers=null
qualname=default
propagate=0

[handler_null]
class = logging.NullHandler
formatter = null
level = CRITICAL
args=()

[formatter_syslog]
format=%(asctime)s %(threadName)s %(filename)s@%(lineno)d %(message)s
datefmt=%b %d %H:%M:%S
```