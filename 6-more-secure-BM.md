Tips for securing Bitmessage
=======================================

# Method 1, Firejail + chroot
- create a chroot and put python binaries, shell, firejail and Bitmessage source code in it
- create a low-priveleged user and change the owner of the chroot to that user
- create a firejail profile outside the chroot and one inside the chroot, restricting that user's access
- use the restrictive firejail the mount the chroot, then use the firejail inside the chroot to run Bitmessage

# Method 2, Firejail
- create a firejail profile for Bitmessagemain.py restricting file access to the keys.dat folder
- copy over the required binaries as if creating a chroot
- add network interface restrictions so Bitmessage can only access the Tor proxy
- create a new user that will only have access to that folder and invoke Bitmessage as that user
- you could even try running Bitmessage as user <nobody>
- firejail instances can be embedded in parent firejail instances for extra armor

# Method 3, Docker
- set up a Docker container and run Bitmessage inside it
- Docker allows easy isolation using Linux kernel namespaces - it is very secure

# Method 4, Systemd (if your distro has it)
- use Systemd-nspawn to isolate the Bitmessage directory in a container
- copy over the necessary binaries and libraries to the restricted folder

# Method 5, Apparmor (if your distro has it)
- Apparmor can be very secure but you might need to read the manual to set it up for your particular distro
- Firejail or Docker are probably going to be easier to implement, and can be combined with Apparmor

GONQ broadcast
BM-5oQUrQbR4xu2rnBFGM2A6BLkJMfYvsw