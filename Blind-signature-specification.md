# Blind signatures
**WIP**

Blind signatures use the following primitives which need to be saved into a file system or transferred over the network:

**BigNum (32B)**: since the curve is secp256k1, the bignums are 256bits long (32 bytes). They are serialized using OpenSSL's BN_bn2binpad

**Eliptic curve points (33B)**: again, since we're using secp256k1, the coordinates X and Y are 32 bytes. We use compressed format compatible with OpenSSL's EC_POINT_set_compressed_coordinates. I tried to replicate how it's stored in other places and how OpenSSL behaves, so the data should be loadable by other software just like a bitcoin private key
- the first byte is 0x02 or 0x03 (which determines which of the two Y coordinates it uses, the difference is that one is even and one is odd. Other bits are ignored by Bitmessage
- the next 32 bytes are the X coordinate (which is a bignum)

**Private key (32B)**: private key is a 256 bit bignum

**Public key (35B)**: public key is two bytes of parameters followed by an EC Point as described above
- first byte is expiration, split into two 4-bit parts. The lower 4 bits are month (0 - 11), and the higher 4 bits are year (add 2020 to get the actual year). For example 0x03 is April 2020.
- second byte is value (1-255)
- the rest of the bytes (33B) is an EC point

**Random integer provided by the signer to the requester during signing (33B)**: is an EC point

**Blinded signature(32B)**: Blinded signature is a 256bit bignum

**Unblinded Signature (65B)**: is a bignum (32B) followed by an EC point (33B)

**Signature chain (100 bytes per level)**:
- Begins with the CA pubkey (35 bytes)
- on the next levels the intermediary pubkey (35B) and a signature of that pubkey signed by the parent pubkey (65B)
- and on the last level, the signature of the message signed by the parent pubkey (65B)