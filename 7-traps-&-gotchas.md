this is a woefully incomplete attempt to list a few "rough edges" of pyBM, which may cause confusion to newbies:

***


*  https://github.com/Bitmessage/PyBitmessage/blob/v0.6/src/bitmessagecli.py is a CLI app to run BM which works only in /src/  move it away into some "attic" dir to avoid confusion and unclutter /src/ from "chaff".

* bitmessageui.ui  in v0.6 is outdated and not used long ago. If you want to edit xml, use https://github.com/g1itch/PyBitmessage/tree/ui-refactoring
Though it has translation problems. using 

     `    pyuic4  /src/bitmessageqt/bitmessageui.ui   -o  src/bitmessageqt/bitmessageui.py ` after editing with Qt4-Designer  will break the whole app now.

* even though running   ` ./bitmessagemain.py ` straight away used to work, it is better to run   ` python2 setup.py build  `  beforehand to avoid crashes e.g. on Fedora.

* it behooves you to use `  export BITMESSAGE_HOME="/mnt/……  `  rather than  portable mode in Linux. This actually avoids crashes.

* **subscriptions** are **lost** after deletion of messages.dat which might not be obvious. make API script to auto keep.
